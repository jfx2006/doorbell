from setuptools import setup

from pip._internal.req import parse_requirements

requirements = [
    req.requirement for req in parse_requirements("requirements.in", session=False)
]

setup(
    name="doorbell",
    version="0.1",
    packages=[""],
    url="https://gitlab.com/jfx2006/doorbell",
    license="MIT",
    author="JFX2006",
    author_email="jfx@calypsoblue.org",
    description="Simple MQTT triggered doorbell",
    install_requires=requirements,
)
