#!/usr/bin/python3
"""
Usage:

Set the following environment variables. Required values are
annotated with an asterisk(*):

MQTT_CLIENT_ID - default system hostname
MQTT_USERNAME *
MQTT_PASSWORD *
MQTT_SERVER *
MQTT_PORT - default 1883
MQTT_TLS - default 0 (False), set to 1 to enable
MQTT_TOPIC *
MQTT_AUDIO_FILE - default is doorbell.mp3. Can be an absolute path or relative to source directory

"""
import logging
import os
import socket
import ssl
import sys
from datetime import datetime as dt
from pathlib import Path

import certifi
import paho.mqtt.client as mqtt
from just_playback import Playback
from systemd.journal import JournalHandler

logger = logging.getLogger("doorbell.py")
journal_handler = JournalHandler()
journal_handler.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(journal_handler)
logger.setLevel(logging.INFO)

HERE = Path(__file__).parent

PRESENCE_TOPIC_BASE = "calypsoblue/doorbell"
PRESENCE_INTERVAL = 60


class EnvMissing(Exception):
    pass


class MyMQTTClass(mqtt.Client):
    defaults = {
        "client_id": socket.gethostname(),
        "username": None,
        "password": None,
        "server": None,
        "port": 1883,
        "tls": 0,
        "topic": None,
        "audio_file": "doorbell.mp3",
    }

    def __init__(self, *args, **kwargs):
        self.audio_file = None
        self.playback = None

        missing = []
        for k, v in self.defaults.items():
            value = kwargs.get(k, v)
            if value is None:
                value = v
            if value is None:
                missing.append(f"MQTT_{k.upper()}")
            setattr(self, k, value)

            if k != "password":
                logger.info(f"Using {k} = {value}")

        if missing:
            missing_str = " ".join(missing)
            raise EnvMissing(f"Required environment variables not set: {missing_str}")

        super().__init__(client_id=self.client_id, protocol=mqtt.MQTTv5)
        self.enable_logger(logger)
        self.username_pw_set(self.username, self.password)
        self.presence_topic= f"{PRESENCE_TOPIC_BASE}/{self.client_id}"
        self.last_presence = 0

    def publish_presence(self):
        now = dt.utcnow()
        if now.timestamp() - self.last_presence > PRESENCE_INTERVAL:
            self.publish(self.presence_topic, now.ctime())
            self.last_presence = now.timestamp()

    def on_publish(self, client, userdata, mid):
        logger.info(f"Published {mid}.")

    def on_connect(self, mqttc, userdata, flags, reasonCode, properties):
        self.will_set(self.presence_topic, None)

    def on_connect_fail(self, mqttc, userdata):
        logger.info("Connect failed")

    def on_message(self, mqttc, userdata, msg):
        logger.debug(f"{msg.topic} {str(msg.qos)} {str(msg.payload)}")
        if msg.payload == b"on":
            logger.info("Doorbell triggered!")
            self.buzzer()

    def on_subscribe(self, mqttc, obj, mid, reasonCodes, properties):
        reasons = "\n".join([code.getName() for code in reasonCodes])
        logger.info(f"Subscribed: {str(mid)} {reasons} {str(properties)}")
        self.publish_presence()

    def on_log(self, mqttc, obj, level, string):
        pass
        # logger.info(string)

    def init_audio(self):
        logger.info("Initialize audio")
        audio_file = Path(self.audio_file)
        if not audio_file.is_absolute():
            audio_file = HERE / audio_file

        self.playback = Playback(str(audio_file))

    def buzzer(self):
        self.playback.play()
        self.playback.seek(0)

    def run(self):
        self.init_audio()
        if bool(self.tls):
            self.tls_set(certifi.where(), tls_version=ssl.PROTOCOL_TLSv1_2)
            self.tls_insecure_set(True)
        self.connect(self.server, int(self.port), 60)
        self.subscribe(self.topic)

        self.buzzer()

        self.loop_start()
        while True:
            self.publish_presence()



def get_environ_vars():
    keys = (
        "MQTT_CLIENT_ID",
        "MQTT_USERNAME",
        "MQTT_PASSWORD",
        "MQTT_SERVER",
        "MQTT_PORT",
        "MQTT_TLS",
        "MQTT_TOPIC",
        "MQTT_AUDIO_FILE",
    )
    return dict([(k.lower()[5:], os.environ.get(k, None)) for k in keys])


def usage():
    print(__doc__)


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] in ("-h", "--help"):
        usage()

    os.chdir(HERE)
    mqttc = MyMQTTClass(**get_environ_vars())
    rv = mqttc.run()

    logger.info(f"rv: {str(rv)}")
    sys.exit(rv)
